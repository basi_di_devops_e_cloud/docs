******

<details>
<summary>Clonare il progetto e creare il proprio repository Git </summary>
 <br />

**passaggi:**

```sh
# clona il repository e cambia nella directory del progetto
git clone https://gayanseneviratne@dev.azure.com/gayanseneviratne/Test/_git/Artifact
cd Artifact

```

</details>

******

<details>
<summary> Costruire l'artefatto jar </summary>
 <br />

**passaggi:**

```sh
gradle build

```

</details>

******

<details>
<summary> Eseguire i test </summary>
 <br />

**passaggi:**
```sh
# individua il file AppTest.java nella cartella src/test/java, alla riga 22 e correggi il test
boolean result = myApp.getCondition(true); 

# esegui i test
gradle test

```

</details>

******

<details>
<summary> Clean e Build l'App </summary>
 <br />

**passaggi:**
```sh
gradle clean 
gradle build

```

</details>

******