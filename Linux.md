### Comandi per il corso Basi di DevOps e Cloud ###
<br />

Operazioni Generali:
- `clear` = Pulisce il terminale

Operazioni sulle Directory:
- `pwd` = Mostra la directory corrente. Esempio di Output: `/home/magnetico`
- `ls` = Elenca cartelle e file. Esempio di Output: `Desktop  Downloads  Pictures  Documents`
- `cd [dirname]` = Cambia directory in [dir]
- `mkdir [dirname]` = Crea una directory [dirname]
- `cd ..` = Torna alla directory superiore

Operazioni sui File:
- `touch [filename]` = Crea [filename]
- `rm [filename]` = Elimina [filename]
- `rm -r [dirname]` = Elimina una directory non vuota e tutti i file al suo interno
- `rm -d [dirname]` o `rmdir [dirname]` = Elimina una directory vuota

Navigazione nel File System:
- `cd usr/local/bin` = Naviga tra più directory (percorso relativo - relativo alla directory corrente). Vai alla directory bin
- `cd ../..` = Torna su di 2 gerarchie, quindi vai alla directory 'usr'
- `cd /usr` = Alternativa per andare direttamente alla directory 'usr' (percorso assoluto)
- `cd [percorso assoluto]` = Spostati in qualsiasi posizione fornendo il percorso completo
- `cd /home/magnetico` = Vai alla mia directory home (percorso assoluto)
- `cd ~` = Scorciatoia per andare alla directory home
- `ls /etc/network` = Elenca cartelle e file della directory 'network'

Altre Operazioni su File e Directory:
- `mv [filename] [new_filename]` = Rinomina il file con un nuovo nome
- `cp -r [dirname] [new_dirname]` = Copia dirname in new_dirname ricorsivamente, inclusi i file
- `cp [filename] [new_filename]` = Copia filename in new_filename

Altri comandi utili:
- `ls -R [dirname]` = Mostra directory e file, ma anche le sottodirectory e i file
- `history` = Fornisce un elenco di tutti i comandi precedentemente digitati nella sessione terminale corrente
- `history 20` = Mostra l'elenco degli ultimi 20 comandi
- `CTRL + r` = Cerca nella cronologia
- `CTRL + c` = Ferma il comando corrente
- `CTRL + SHIFT + v` = Incolla il testo copiato nel terminale
- `ls -a` = Vedi anche i file nascosti
- `cat [filename]` = Mostra il contenuto del file
- `cat .bash_history` = Esempio 1: Mostra il contenuto del file
- `cat Documents/java-app/Readme.md` = Esempio 2: Mostra il contenuto del file

Mostra Informazioni sul Sistema Operativo:
- `uname -a` = Mostra il sistema e il kernel
- `cat /etc/os-release` = Mostra le informazioni sul sistema operativo
- `lscpu` = Mostra informazioni sull'hardware, ad esempio quanti CPU ci sono, ecc.
- `lsmem` = Mostra informazioni sulla memoria

Esegui comandi come superutente:
- `sudo [some command]` = Permette agli utenti normali di eseguire programmi con i privilegi di sicurezza del superutente o root
- `su - admin` = Passa dall'utente magnetico all'utente admin
</details>

******

<details>
<summary>Gestione dei pacchetti</summary>
<br />

Gestore di pacchetti APT:
- `sudo apt search [package_name]` = Cerca un dato pacchetto
- `sudo apt install [package_name]` = Installa un dato pacchetto
- `sudo apt install [package_name] [package_name2]` = Installa più pacchetti con un comando
- `sudo apt remove [package_name]` = Rimuove un pacchetto installato
- `sudo apt update` = Aggiorna l'indice dei pacchetti. Recupera le ultime modifiche dai repository APT

Gestore di pacchetti APT-GET:
- `sudo apt-get install [package_name]` = Installa un pacchetto con il gestore di pacchetti apt-get

Gestore di pacchetti SNAP:
- `sudo snap install [package_name]` = Installa un dato pacchetto

</details>

******

<details>
<summary>Vim</summary>
<br />

Installa Vim, se non è disponibile:
- `sudo apt install vim` = Cerca un dato pacchetto

Ci sono 2 Modalità:
- Modalità Comando: modalità predefinita, tutto viene interpretato come un comando
- Modalità Inserimento: Permette di inserire testo

Comandi Vim:
- `vim [filename]` = Apri un file con Vim
- `Premi il tasto i` = Passa alla Modalità Inserimento
- `Premi il tasto esc` = Passa alla Modalità Comando
- `Digita :wq` = Salva il file su disco ed esci da Vim
- `Digita :q!` = Esci da Vim senza salvare le modifiche
- `Digita dd` = Elimina l'intera riga
- `Digita d10` = Elimina le prossime 10 righe
- `Digita u` = Annulla
- `Digita A` = Vai alla fine della riga e passa alla modalità inserimento
- `Digita 0` = Vai all'inizio della riga
- `Digita $` = Vai alla fine della riga
- `Digita 12G` = Vai alla riga 12
- `Digita 16G` = Vai alla riga 16
- `Digita /pattern` = Cerca un pattern, ad esempio `/nginx`
    - `Digita n` = Vai al prossimo match
    - `Digita N` = Cerca nella direzione opposta
- `Digita :%s/old/new` = Sostituisci 'old' con 'new' in tutto il file

</details>

******


<details>
<summary>Utenti e gruppi</summary>
 <br />

**Posizione dei File di Controllo degli Accessi:**
- /etc/passwd
- /etc/shadow
- /etc/group
<!-- -->
- `sudo adduser [username]` = Crea un nuovo utente
- `sudo passwd [username]` = Cambia la password di un utente
- `su - [username]` = Accedi come username ('su' = abbreviazione di substitute o switch user)
- `su -` = Accedi come root
<!-- -->
- `sudo groupadd [groupname]` = Crea un nuovo gruppo (Il sistema assegna il prossimo GID disponibile)
- `sudo adduser [username]` = Passa alla Modalità Inserimento

**Nota 2 diversi comandi per Utenti/Gruppi:**<br />
`adduser`, `addgroup`, `deluser`, `delgroup` = comandi interattivi, più user-friendly<br />
`useradd`, `groupadd`, `userdel`, `groupdel` = utility di basso livello, necessitano di maggiori informazioni fornite dall'utente

- `sudo usermod [OPTIONS] [username]` = Modifica un account utente
- `sudo usermod -g devops tom` = Assegna 'devops' come gruppo primario per l'utente 'tom'
- `sudo delgroup tom` = Rimuove il gruppo 'tom'
- `groups` = Mostra i gruppi a cui appartiene l'utente attualmente connesso
- `groups [username]` = Mostra i gruppi dell'utente specificato
- `sudo useradd -G devops nicole` = Crea l'utente 'nicole' e aggiungi nicole al gruppo 'devops' (-G = gruppo secondario, non primario)
- `sudo gpasswd -d nicole devops` = Rimuove l'utente 'nicole' dal gruppo 'devops'

</details>

******

<details>
<summary>Permessi sui file</summary>
 <br />

- `ls -l` = Stampa i file in un formato di elenco lungo, è possibile vedere la proprietà e i permessi del file

**Proprietà:**
- `sudo chown [username]:[groupname] [filename]` = Cambia la proprietà
- `sudo chown tom:admin test.txt` = Cambia la proprietà del file 'test.txt' a 'tom' e il gruppo a 'admin'
- `sudo chown admin test.txt` = Cambia la proprietà del file 'test.txt' all'utente 'admin'
- `sudo chgrp devops test.txt` = Assegna il gruppo 'devops' come proprietario del file test.txt

**Possibili Permessi sui File (Simbolici):**
- r = Lettura
- w = Scrittura
- x = Esecuzione
- '-' = Nessun permesso

**Cambia i Permessi sui File**

- `chmod [options] [permissions] [filename]` = Cambia i permessi sui file
- `chmod u+x file.sh` = Concede il permesso di esecuzione solo all'utente proprietario
- `chmod u+rwx,g+rw,o-rwx file.sh` = Specifica permessi per utente/gruppo/altri

**Possibili Permessi sui File (Ottali):**
- 4 = Lettura
- 2 = Scrittura
- 1 = Esecuzione
- 0 = Nessun permesso

**Somma e usa permessi ottali:**
- 4+2+1 = 7 (rwx)
- 4+2+0 = 6 (rw-)
- 4+0+0 = 4 (r--)
- 0+0+0 = 0 (---)

- `chmod 740 test.txt` = L'utente può leggere/scrivere/eseguire; Il gruppo può leggere; Altri non possono fare nulla
- `chmod 600 test.txt` = L'utente può leggere/scrivere; Il gruppo non può fare nulla; Altri non possono fare nulla

**Controlla il Permesso Esecutivo per Script:**
- `ls -l test.txt` = Controlla se il permesso di esecuzione è già concesso (es., `-rwx------`)
- `./test.txt` = Esegui lo script. Funziona solo se il permesso di esecuzione è concesso

</details>

******

<details>
<summary>Variabili d'Ambiente</summary>
<br />

_Le variabili memorizzano informazioni. Le variabili d'ambiente sono disponibili per tutto l'ambiente._
_Una variabile d'ambiente consiste in una coppia _nome=valore_._

**Variabili d'Ambiente Esistenti:**
- `SHELL=/bin/bash`= programma shell predefinito, in questo caso bash
- `HOME=/home/nana`= directory home dell'utente corrente
- `USER=nana` = utente attualmente connesso
<!-- -->
- `printenv` = Elenca tutte le variabili d'ambiente
- `printenv | less` = Elenca tutte le variabili d'ambiente con il programma less
- `printenv [variabile d'ambiente]` = Mostra il valore della variabile d'ambiente specificata, ad esempio `printenv USER`
- `printenv | grep USER` = Filtra le variabili d'ambiente che contengono 'USER' nel nome
<!-- -->
- `echo $USER` = Stampa il valore della variabile d'ambiente USER

**Creare Proprie Variabili d'Ambiente:**
- `export DB_USERNAME=dbuser` = Imposta la variabile d'ambiente 'DB_USERNAME' con il valore 'dbuser'
- `export DB_PASSWORD=secretpwdvalue` = Imposta la variabile d'ambiente 'DB_PASSWORD' con il valore 'secretpwdvalue'
- `export DB_NAME=mydb` = Imposta la variabile d'ambiente 'DB_NAME' con il valore 'mydb'
- `printenv | grep DB` = Filtra le variabili d'ambiente che contengono 'DB' nei caratteri
- `export DB_NAME=newdbname` = Imposta la variabile d'ambiente 'DB_NAME' con il nuovo valore 'newdbname'

**Eliminare Variabili d'Ambiente:**
- `unset DB_NAME` = Elimina la variabile con il nome 'DB_NAME'

**Rendere Persistenti le Variabili d'Ambiente:**

Rendere Persistenti le Variabili d'Ambiente con il File di Configurazione Specifico della Shell:
_Le variabili d'ambiente impostate nel terminale sono disponibili solo nella sessione del terminale corrente._

Aggiungi le variabili d'ambiente al file '~/.bashrc' o al file 'rc' specifico della tua shell. Le variabili impostate in questo file vengono caricate ogni volta che si entra in una shell di login bash.
- `export DB_USERNAME=dbuser`
- `export DB_PASSWORD=secretvl`
- `export DB_NAME=mydb`
Di nuovo nel terminale:
- `source ~/.bashrc` = Carica le nuove variabili d'ambiente nella sessione della shell corrente

Rendere Persistenti le Variabili d'Ambiente a Livello di Sistema:
- ~/.bashrc = specifico per l'utente
- /etc/environment = a livello di sistema, significa che tutti gli utenti avranno accesso alle variabili

**Variabile d'Ambiente PATH:**
- `PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin` = Elenco delle directory con file eseguibili, separati da ':'. Indica alla shell in quali directory cercare l'eseguibile in risposta al nostro comando eseguito
- `PATH=$PATH:/home/nana` = Aggiunge la cartella /home/nana al valore esistente di $PATH

</details>

******

<details>
<summary>SSH</summary>
 <br />

- `sudo apt install openssh-server` = Installa il server SSH
- `sudo systemctl enable ssh` = Abilita il server SSH
- `sudo systemctl start ssh` = Avvia il server SSH
- `ssh [username]@[ip address]` = Connessione SSH

</details>


