### 1. Configurazione iniziale

```sh
# Imposta il nome utente globale
git config --global user.name "Il Tuo Nome"

# Imposta l'email globale
git config --global user.email "tuoemail@example.com"
```
Questi comandi configurano Git per utilizzare il tuo nome e email in tutti i tuoi repository.

### 2. Creare un nuovo repository

Per iniziare a gestire un progetto con Git, devi prima creare un repository.

```sh
# Crea un nuovo repository nella directory corrente
git init
```
Questo comando inizializza un nuovo repository Git nella directory corrente. Creerà una cartella `.git` che contiene tutti i file necessari per il repository.

```sh
# Crea una nuova cartella e inizializza un repository Git al suo interno
mkdir nome-repo
cd nome-repo
git init
```
Questi comandi creano una nuova directory `nome-repo`, ci entrano e inizializzano un repository Git al suo interno.

### 3. Clonare un repository esistente

Se vuoi iniziare a lavorare su un progetto esistente, puoi clonare il repository remoto.

```sh
# Clona un repository remoto
git clone https://github.com/utente/repository.git
```
Questo comando crea una copia locale del repository remoto specificato dall'URL.

### 4. Controllare lo stato del repository

È utile controllare lo stato del repository per vedere quali modifiche sono state apportate.

```sh
# Mostra lo stato dei file nel repository
git status
```
Il comando `git status` mostra quali file sono stati modificati, quali sono in area di staging e quali non sono monitorati da Git.

### 5. Aggiungere file all'area di staging

Prima di poter creare un commit, devi aggiungere i file modificati all'area di staging.

```sh
# Aggiungi un singolo file all'area di staging
git add nomefile
```
Questo comando aggiunge il file specificato all'area di staging.

```sh
# Aggiungi tutti i file all'area di staging
git add .
```
Questo comando aggiunge tutti i file modificati e nuovi all'area di staging.

### 6. Creare un commit

Una volta che i file sono nell'area di staging, puoi creare un commit per salvare le modifiche.

```sh
# Effettua un commit con un messaggio descrittivo
git commit -m "Messaggio del commit"
```
Il comando `git commit` crea un commit con il messaggio specificato, che descrive le modifiche apportate.

### 7. Vedere la cronologia dei commit

Per vedere la cronologia dei commit effettuati nel repository, puoi usare:

```sh
# Mostra la cronologia dei commit
git log
```
Questo comando mostra una lista dettagliata di tutti i commit.

```sh
# Mostra la cronologia dei commit con un formato compatto
git log --oneline
```
Questo comando mostra la cronologia dei commit in un formato più compatto, con un singolo commit per linea.

### 8. Gestire i branch

I branch ti permettono di lavorare su differenti versioni del tuo progetto in parallelo.

```sh
# Elenca tutti i branch
git branch
```
Questo comando mostra una lista di tutti i branch nel repository, indicando il branch corrente con un asterisco (*).

```sh
# Crea un nuovo branch
git branch nome-branch
```
Questo comando crea un nuovo branch chiamato `nome-branch`.

```sh
# Passa a un altro branch
git checkout nome-branch
```
Questo comando ti sposta al branch `nome-branch`.

```sh
# Crea e passa a un nuovo branch
git checkout -b nome-branch
```
Questo comando crea un nuovo branch chiamato `nome-branch` e ci sposta immediatamente su di esso.

```sh
# Unisce un branch al branch corrente
git merge nome-branch
```
Questo comando unisce il branch `nome-branch` al branch corrente.

```sh
# Elimina un branch
git branch -d nome-branch
```
Questo comando elimina il branch `nome-branch`.

### 9. Lavorare con i repository remoti

Puoi lavorare con repository remoti per collaborare con altri.

```sh
# Mostra i repository remoti
git remote -v
```
Questo comando mostra tutti i repository remoti configurati per il tuo repository locale.

```sh
# Aggiungi un repository remoto
git remote add origin https://github.com/utente/repository.git
```
Questo comando aggiunge un nuovo repository remoto chiamato `origin` con l'URL specificato.

```sh
# Pusha i cambiamenti al repository remoto
git push origin master
```
Questo comando invia i commit dal branch `master` locale al branch `master` remoto.

```sh
# Fetch dei cambiamenti dal repository remoto
git fetch
```
Questo comando scarica i cambiamenti dal repository remoto ma non li unisce al branch corrente.

```sh
# Pull dei cambiamenti dal repository remoto
git pull
```
Questo comando scarica e unisce i cambiamenti dal repository remoto al branch corrente.

### 10. Annullare cambiamenti

Se hai bisogno di annullare delle modifiche, ci sono vari modi per farlo.

```sh
# Annulla le modifiche nell'area di staging
git reset nomefile
```
Questo comando rimuove `nomefile` dall'area di staging.

```sh
# Ripristina un file allo stato dell'ultimo commit
git checkout -- nomefile
```
Questo comando ripristina `nomefile` all'ultimo commit, annullando tutte le modifiche non committate.

```sh
# Annulla l'ultimo commit mantenendo le modifiche nell'area di staging
git reset --soft HEAD~
```
Questo comando annulla l'ultimo commit ma mantiene le modifiche nell'area di staging.

```sh
# Annulla l'ultimo commit rimuovendo le modifiche
git reset --hard HEAD~
```
Questo comando annulla l'ultimo commit e rimuove tutte le modifiche.

### 11. Stash delle modifiche

Se devi temporaneamente salvare le modifiche non committate senza fare un commit, puoi usare lo stash.

```sh
# Salva le modifiche non committate e pulisce la working directory
git stash
```
Questo comando salva le modifiche non committate e ripristina la working directory allo stato dell'ultimo commit.

```sh
# Mostra gli stash salvati
git stash list
```
Questo comando mostra una lista di tutte le modifiche stashed.

```sh
# Applica l'ultimo stash e lo rimuove dalla lista degli stash
git stash pop
```
Questo comando applica le modifiche dall'ultimo stash e lo rimuove dalla lista.

```sh
# Applica l'ultimo stash senza rimuoverlo dalla lista
git stash apply
```
Questo comando applica le modifiche dall'ultimo stash senza rimuoverlo dalla lista.

```sh
# Elimina uno specifico stash
git stash drop stash@{0}
```
Questo comando elimina lo stash specificato dalla lista.

### 12. Taggare i commit

I tag sono utili per marcare punti specifici nella cronologia dei commit.

```sh
# Crea un nuovo tag annotato
git tag -a v1.0 -m "Versione 1.0"
```
Questo comando crea un nuovo tag `v1.0` con un messaggio annotato.

```sh
# Lista di tutti i tag
git tag
```
Questo comando mostra una lista di tutti i tag nel repository.

```sh
# Pusha i tag al repository remoto
git push origin --tags
```
Questo comando invia tutti i tag al repository remoto.

### 13. Visualizzare differenze

Per vedere le differenze tra varie versioni dei tuoi file, puoi usare `git diff`.

```sh
# Mostra le differenze tra il working directory e l'area di staging
git diff
```
Questo comando mostra le differenze tra i file modificati e quelli in area di staging.

```sh
# Mostra le differenze tra l'area di staging e l'ultimo commit
git diff --cached
```
Questo comando mostra le differenze tra i file in area di staging e l'ultimo commit.

```sh
# Mostra le differenze tra due branch
git diff branch1..branch2
```
Questo comando mostra le differenze tra due branch specificati.
