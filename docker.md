### Comandi di Base di Docker con Esempi Utilizzando Redis

1. **docker --version**
   - Mostra la versione di Docker installata.
   - Esempio:
     ```sh
     docker --version
     ```
     Output:
     ```sh
     Docker version 20.10.7, build f0df350
     ```

2. **docker pull**
   - Scarica l'immagine di Redis dal Docker Hub.
   - Esempio:
     ```sh
     docker pull redis
     ```
     Output:
     ```sh
     Using default tag: latest
     latest: Pulling from library/redis
     ...
     Digest: sha256:...
     Status: Downloaded newer image for redis:latest
     ```

3. **docker run**
   - Crea e avvia un container da un'immagine.
   - Esempio:
     ```sh
     docker run -d --name my_redis redis
     ```
     Output:
     ```sh
     <container_id>
     ```

4. **docker ps**
   - Mostra i container in esecuzione.
   - Esempio:
     ```sh
     docker ps
     ```
     Output:
     ```sh
     CONTAINER ID   IMAGE     COMMAND                  CREATED         STATUS         PORTS      NAMES
     abc12345       redis     "docker-entrypoint.s…"   2 minutes ago   Up 2 minutes   6379/tcp   my_redis
     ```

5. **docker ps -a**
   - Mostra tutti i container, inclusi quelli fermati.
   - Esempio:
     ```sh
     docker ps -a
     ```
     Output:
     ```sh
     CONTAINER ID   IMAGE     COMMAND                  CREATED         STATUS                     PORTS      NAMES
     abc12345       redis     "docker-entrypoint.s…"   2 minutes ago   Exited (0) 10 seconds ago             my_redis
     ```

6. **docker stop**
   - Ferma un container in esecuzione.
   - Esempio:
     ```sh
     docker stop abc12345
     ```

7. **docker start**
   - Avvia un container fermato.
   - Esempio:
     ```sh
     docker start abc12345
     ```

8. **docker rm**
   - Rimuove un container fermato.
   - Esempio:
     ```sh
     docker rm abc12345
     ```

9. **docker rmi**
   - Rimuove un'immagine.
   - Esempio:
     ```sh
     docker rmi redis
     ```

10. **docker images**
    - Mostra le immagini disponibili localmente.
    - Esempio:
      ```sh
      docker images
      ```
      Output:
      ```sh
      REPOSITORY   TAG       IMAGE ID       CREATED        SIZE
      redis        latest    c5e19df598a8   2 weeks ago    117MB
      ```

11. **docker build**
    - Costruisce un'immagine da un Dockerfile.
    - Esempio:
      Supponendo di avere un `Dockerfile` che utilizza Redis nella directory corrente:
      ```dockerfile
      # Esempio di Dockerfile
      FROM redis
      CMD ["redis-server"]
      ```
      ```sh
      docker build -t my_redis_image .
      ```
      Output:
      ```sh
      Sending build context to Docker daemon  2.048kB
      Step 1/2 : FROM redis
       ---> c5e19df598a8
      Step 2/2 : CMD ["redis-server"]
       ---> Running in abc12345
      Removing intermediate container abc12345
       ---> e1d3d4a5b6c7
      Successfully built e1d3d4a5b6c7
      Successfully tagged my_redis_image:latest
      ```

12. **docker exec**
    - Esegue un comando in un container in esecuzione.
    - Esempio:
      ```sh
      docker exec -it abc12345 redis-cli
      ```

13. **docker logs**
    - Mostra i log di un container.
    - Esempio:
      ```sh
      docker logs abc12345
      ```

14. **docker network create**
    - Crea una rete Docker.
    - Esempio:
      ```sh
      docker network create my_network
      ```
      Output:
      ```sh
      9e8f8d4f6b2c3e7d5f6g7h8i9j0k1l2m
      ```

15. **docker network ls**
    - Elenca le reti Docker.
    - Esempio:
      ```sh
      docker network ls
      ```
      Output:
      ```sh
      NETWORK ID     NAME         DRIVER    SCOPE
      9e8f8d4f6b2c   my_network   bridge    local
      ```

16. **docker network inspect**
    - Mostra i dettagli di una rete Docker.
    - Esempio:
      ```sh
      docker network inspect my_network
      ```

17. **docker network connect**
    - Connetti un container a una rete Docker.
    - Esempio:
      ```sh
      docker network connect my_network abc12345
      ```

18. **docker network disconnect**
    - Disconnetti un container da una rete Docker.
    - Esempio:
      ```sh
      docker network disconnect my_network abc12345
      ```

### Esempio: Due Container che Riescono a Contattarsi tra di Loro Utilizzando Redis

1. **Crea una rete personalizzata**:
   ```sh
   docker network create my_network
   ```

2. **Esegui il primo container Redis e connettilo alla rete**:
   ```sh
   docker run -d --name redis1 --network my_network redis
   ```

3. **Esegui il secondo container Redis e connettilo alla rete**:
   ```sh
   docker run -d --name redis2 --network my_network redis
   ```

4. **Verifica la connessione tra i container**:
   - Esegui un comando di ping da `redis1` a `redis2`:
   ```sh
   docker exec -it redis1 redis-cli -h redis2 ping
   ```
   Output:
   ```sh
   PONG
   ```

### Esempio: Host Port Diverso dal Container Port Utilizzando Redis

1. **Esegui un container Redis con una porta host diversa da quella del container**:
   ```sh
   docker run -d -p 8080:6379 --name my_redis redis
   ```

2. **Verifica che il container sia in esecuzione**:
   ```sh
   docker ps
   ```
   Output:
   ```sh
   CONTAINER ID   IMAGE     COMMAND                  CREATED         STATUS         PORTS                     NAMES
   abc12345       redis     "docker-entrypoint.s…"   2 minutes ago   Up 2 minutes   0.0.0.0:8080->6379/tcp    my_redis
   ```

3. **Accedi al container Redis tramite il client Redis sulla porta host 8080**:
   - Utilizza `redis-cli`:
   ```sh
   redis-cli -p 8080
   ```

### Altri Comandi Utili con Esempi Utilizzando Redis

1. **docker inspect**
   - Visualizza i dettagli di un container o di un'immagine.
   - Esempio:
     ```sh
     docker inspect abc12345
     ```

2. **docker commit**
   - Crea una nuova immagine da un container esistente.
   - Esempio:
     ```sh
     docker commit abc12345 my_redis_image
     ```

3. **docker cp**
   - Copia file o directory tra il file system del container e quello dell'host.
   - Esempio:
     ```sh
     docker cp abc12345:/data/dump.rdb /host/destination/dump.rdb
     ```

4. **docker tag**
   - Assegna un nuovo tag a un'immagine Docker.
   - Esempio:
     ```sh
     docker tag redis myrepo/redis:mytag
     ```

5. **docker push**
   - Carica un'immagine su un registro Docker (come Docker Hub).
   - Esempio:
     ```sh
     docker push myrepo/redis:mytag
     ```

6. **docker login**
   - Effettua il login in un registro Docker.
   - Esempio:
     ```sh
     docker login
     ```